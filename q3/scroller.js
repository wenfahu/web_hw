document.body.onscroll = function() {
    if(document.body.scrollTop < 200){
	console.log('scrolling');
	document.querySelector('#scrollme').style.display = 'none';
    }
    if(document.body.scrollTop >= 200){
	document.querySelector('#scrollme').style.display = 'block';
    }
}

function runScroll() {
    scrollTo(document.body, 0, 600);
}
var scrollme;
scrollme = document.querySelector("#scrollme");
scrollme.addEventListener("click",runScroll,false);

function scrollTo(element, to, duration) {
  if (duration < 0) return;
  var difference = to - element.scrollTop;
  var perTick = difference / duration * 10;

  setTimeout(function() {
    element.scrollTop = element.scrollTop + perTick;
    if (element.scrollTop == to) return;
    scrollTo(element, to, duration - 10);
  }, 10);
}
