var modal = function(){
    this.target = document.getElementsByClassName('tgt')[0];
};

modal.prototype.init = function(obj){
    if(obj.content){
	this.target.innerHTML = obj.content;
    }
    if(obj.draggable === false){
	//
	document.onmousemove = null;
    }
    if(obj.draggable){
	document.onmousemove = _move_elem;
    }
    if(obj.closeKey){
	//
	window.onkeypress = function(event){
	    if(event.keyCode == obj.closeKey){
		var elem = document.getElementsByClassName('tgt')[0];
		elem.parentNode.removeChild(elem);
	    }
	}
    }
};

