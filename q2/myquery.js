var myQuery, $;

(function() {

    myQuery = $ = function(selector) {
        return new MyQuery(selector);
    };

    var MyQuery = function(selector) {
        // Lets make a really simplistic selector implementation for demo purposes
        var TagNameRe = /^((?:[\w\u00c0-\uFFFF\*-]|\\.)+)/;
        var IdRe = /#((?:[\w\u00c0-\uFFFF-]|\\.)+)/;
        var ClassRe = /\.((?:[\w\u00c0-\uFFFF-]|\\.)+)/;
        var nodes;
        if(selector.match(TagNameRe)){
            nodes = document.getElementsByTagName(selector);
        }
        else if(selector.match(IdRe)){
            nodes = document.getElementsById(selector.substr(1));
        }
        else if(selector.match(ClassRe)){
            nodes = document.getElementsByClassName(selector.substr(1));
        }
        else{
            console.log('not supported selector');
        }
        for (var i = 0; i < nodes.length; i++) {
            this[i] = nodes[i];
        }
        this.length = nodes.length;
        return this;
    };

    // Expose the prototype object via myQuery.fn so methods can be added later
    myQuery.fn = MyQuery.prototype = {
        // API Methods
        hide: function() {
            for (var i = 0; i < this.length; i++) {
                this[i].style.display = 'none';
            }
            return this;
        },
        attr: function(attrName, attrValue){
            if(arguments.length == 1){
                var attrs = []
                for(var i = 0; i < this.length; i++){
                    attrs.push(this[i].getAttribute(attrName));
                }
                return attrs;
            }
            else if(arguments.length == 2){
                for(var i = 0; i < this.length; i++){
                    this[i].setAttribute(attrName, attrValue);
                }
                return this;
            }
            else {
                console.log("unknown");
            }
        },
        remove: function() {
            for (var i = 0; i < this.length; i++) {
                this[i].parentNode.removeChild(this[i]);
            }
            return this;
        }
        // More methods here, each using 'return this', to enable chaining
    };

}());